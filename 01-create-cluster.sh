#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "🤖[1/6] creating Civo Cluster: ${CLUSTER_NAME}" 

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes create ${CLUSTER_NAME} --size=${CLUSTER_SIZE} --nodes=${CLUSTER_NODES} --region=${CLUSTER_REGION} --wait

# Cluster size g3.k3s.medium / g3.k3s.large / g3.k3s.xlarge 

echo "📝[2/6]  save the KUBECONFIG file of the ${CLUSTER_NAME} cluster to ./config/k3s.yaml "

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo --region=${CLUSTER_REGION} kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml

echo "🌍[3/6]  get the cluster url (./config/url.txt)"

URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}') 
# Removing ANSI color codes from text stream
NEW_URL=$(echo $URL | sed 's/\x1b\[[0-9;]*m//g')
echo "$NEW_URL"
echo "$NEW_URL" > ./config/url.txt
